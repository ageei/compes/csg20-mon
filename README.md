# Monitors the CS Games 2020 competitions web page

Register to the project's Atom/RSS feed for notifications.

https://gitlab.com/ageei/compes/csg20-mon/commits/master.atom


## Install

1. Generate an SSH keypair with `ssh-keygen -f id_rsa`.
2. Add the base64-encoded SSH key as a CI/CD variable named `SSH_ID_RSA`.
	i. We recommend using `base64 -w0`.
3. Schedule the CI/CD to run every now and then.
